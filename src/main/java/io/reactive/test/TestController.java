package io.reactive.test;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class TestController {

    @GetMapping("/test")
    public Mono<String> getHello(){
        return Mono.just("Hello, World!");
//        return Mono.just(ResponseEntity.ok().body("Hello, World!"));
    }
}
