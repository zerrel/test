---

# Run gatling test
- cd ~/test/gatling-charts-highcharts-bundle-3.1.1/bin
- sh ./gatling.sh
- select desired test
- optional description
  
# View results in HTML template
- cd ~/test/gatling-charts-highcharts-bundle-3.1.1/results