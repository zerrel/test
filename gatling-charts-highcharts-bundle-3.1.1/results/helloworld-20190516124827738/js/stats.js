var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "500",
        "ok": "483",
        "ko": "17"
    },
    "minResponseTime": {
        "total": "88",
        "ok": "220",
        "ko": "88"
    },
    "maxResponseTime": {
        "total": "601",
        "ok": "601",
        "ko": "257"
    },
    "meanResponseTime": {
        "total": "414",
        "ok": "421",
        "ko": "219"
    },
    "standardDeviation": {
        "total": "110",
        "ok": "105",
        "ko": "50"
    },
    "percentiles1": {
        "total": "409",
        "ok": "421",
        "ko": "243"
    },
    "percentiles2": {
        "total": "514",
        "ok": "517",
        "ko": "249"
    },
    "percentiles3": {
        "total": "579",
        "ok": "580",
        "ko": "252"
    },
    "percentiles4": {
        "total": "595",
        "ok": "595",
        "ko": "256"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 483,
        "percentage": 97
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 17,
        "percentage": 3
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "500",
        "ok": "483",
        "ko": "17"
    }
},
contents: {
"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "500",
        "ok": "483",
        "ko": "17"
    },
    "minResponseTime": {
        "total": "88",
        "ok": "220",
        "ko": "88"
    },
    "maxResponseTime": {
        "total": "601",
        "ok": "601",
        "ko": "257"
    },
    "meanResponseTime": {
        "total": "414",
        "ok": "421",
        "ko": "219"
    },
    "standardDeviation": {
        "total": "110",
        "ok": "105",
        "ko": "50"
    },
    "percentiles1": {
        "total": "409",
        "ok": "421",
        "ko": "243"
    },
    "percentiles2": {
        "total": "514",
        "ok": "517",
        "ko": "249"
    },
    "percentiles3": {
        "total": "579",
        "ok": "580",
        "ko": "252"
    },
    "percentiles4": {
        "total": "595",
        "ok": "595",
        "ko": "256"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 483,
        "percentage": 97
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 17,
        "percentage": 3
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "500",
        "ok": "483",
        "ko": "17"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
