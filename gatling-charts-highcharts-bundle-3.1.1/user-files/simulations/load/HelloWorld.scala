package load // 1

import io.gatling.core.Predef._ // 2
import io.gatling.http.Predef._
import scala.concurrent.duration._

class HelloWorld extends Simulation { // 3

  val httpProtocol = http
    .baseUrl("http://localhost:8040") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val headers_10 = Map("Content-Type" -> "application/x-www-form-urlencoded") // Note the headers specific to a given request

  val scn = scenario("HelloWorld") // 7
    .exec(http("request_1") // 8
      .get("/test")) // 9

  setUp( // 11
    scn.inject(atOnceUsers(500)) // 12
  ).protocols(httpProtocol) // 13
}