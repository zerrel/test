/*
 * Copyright 2011-2019 GatlingCorp (https://gatling.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package load

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class BootLoadTest extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:8070") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val headers_10 = Map("Content-Type" -> "application/x-www-form-urlencoded") // Note the headers specific to a given request

  val threadGroup = repeat(10) {
    exec(http("getLiteratureById")
      .get("/literature/search/id=1"))
    .pause(1 second, 2 seconds)
  }

  val scn = scenario("threadGroup").exec(threadGroup)

  setUp(scn.inject(
      nothingFor(2 seconds),
      rampUsers(2000) during (30 seconds))
    .protocols(httpProtocol))
}
